var express = require("express");
var router = express.Router();
var faker = require("faker");
require("dotenv").config();

var transformer = require("@smartserv/transformer");

let Account = transformer.Schemas.Account;
let AccountService = transformer.Services.AccountService;
/* GET home page. */

router.get("/", (req, res, next) => {
  res.send("Hello!");
});

router.get("/users", function (req, res, next) {
  try {
    let data = [];
    let i = 0;
    while (i < 15) {
      let [id, name, desc] = [
        faker.random.number(),
        faker.name.findName() + " " + faker.name.lastName(),
        faker.name.jobTitle(),
      ];
      i++;
      data.push({ id, name, desc });
      console.log(data);
    }
    res.send({ data: data });
  } catch (error) {
    res.send({ result: error });
  }
});

router.get("/transformer/users", async (req, res) => {
  //add logic to insert into db
  console.log(transformer);
  try {
    let query = { limit: 10 };
    const accountQuery = new Account(query);
    const accounts = await AccountService.find(accountQuery);
    console.log(accounts);
    res.send(accounts).status(200);
  } catch (error) {
    // console.log(Account, AccountService);
  }
});

module.exports = router;
